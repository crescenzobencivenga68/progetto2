<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Posts;
use Illuminate\Support\Facades\Http;

class PostsPostsController extends Controller
{
    public function list(){
        return response()->json(Posts::get(),200);
    }
}
