<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class postsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Posts::factory()->count(50)->create();
    }
}
